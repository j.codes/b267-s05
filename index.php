<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S05</title>
</head>
<body>
	<?php session_start(); ?>

	<?php if (isset($_SESSION['user'])): ?>

	<?php foreach($_SESSION['user'] as $id => $user): ?>

		<form method = "POST" action = "./server.php">
			<input type="hidden" name="action" value="logout">
			<p>Hello, <?php echo $user ->email; ?></p>
			<br>
			<button type="submit">Logout</button>
		</form>
	<?php endforeach; ?>

	<?php else: ?> 

	<form method = "POST" action = "./server.php">
		<input type="hidden" name="action" value="login">
		Email: <input type="text" name="email" required>
		Password: <input type="password" name="password" required>
			<button type="submit">Login</button>
	</form>

	<?php endif;?> 
</body>
</html>